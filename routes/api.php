<?php

/** Created by Ramadhani Widodo
 * Created Date: 06 July 2017
 * Description: routes for restAPI mCoin
 **/

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api','cors']], function () {
    Route::post('v1/authenticate', 'APIController@login');
});

// Custom routes for mCoin
Route::post('v1/users', 'UserController@store');
Route::get('v1/users/{id}', 'UserController@show');
Route::post('v1/users/{id}', 'UserController@update');
Route::delete('v1/users/{id}', 'UserController@delete');
