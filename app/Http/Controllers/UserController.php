<?php

/** Created by Ramadhani Widodo
 * Created Date: 06 July 2017
 * Description: User Controller for restAPI mCoin
 **/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	App\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
	/**
    * Display a listing of the resource.
    *
    * @return Response
    */
   	public function index() {
    	//
    	return User::all();
   	}

    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(Request $request) {
    	 $validator = Validator::make($request->all(), [
	        'fullname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
	    ]);

    	if ($validator->fails()) {
        	return response()->json(
        		[
    			'status' => 'ERROR',
                'message' => "User already exists" 
                ], 400);
    	} 

    	$user = new User();
    	$user->email = $request->get('email');
    	$user->password = $request->get('password');
    	$user->fullname = $request->get('fullname');
    	$user->birth_date = $request->get('birth_date');

    	if ($request->hasFile('photo')) {
    		$file = $request->file('photo');
    		if(!$file->isValid()) {
				return response()->json(['invalid_file_upload'], 400);
			}
			
			$path = public_path() . '/uploads/';
			$file->move($path, $file->getClientOriginalName() );
			$user->photo = $file->getClientOriginalName();
    	}

    	$success = $user->save();

    	return response()->json(
    			[
    			'status' => 'OK',
                'message' => "User successfully registered" 
                ], 201);
    }
   
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id) {
   		$user = User::find($id);

   		if ($user == NULL) {
   			return response()->json(
   				[
    			'status' => 'ERROR',
                'message' => "User does no exist" 
                ], 400);
   		}

		return response()->json(
				[
    			'status' => 'OK',
                'data' => $user 
                ], 201);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(Request $request, $id) {
    	$user = User::find($id);

    	if ($user == NULL) {
   			return response()->json(
   				[
   				'status' => 'ERROR',
   				'message' => 'User does not exist'
   				], 400);
   		}

    	$user->email = $request->get('email');
    	$user->password = $request->get('password');
    	$user->fullname = $request->get('fullname');
    	$user->birth_date = $request->get('birth_date');

    	if ($request->hasFile('photo')) {
    		$file = $request->file('photo');
    		if(!$file->isValid()) {
				return response()->json(['invalid_file_upload'], 400);
			}
			
			$path = public_path() . '/uploads/';
			$file->move($path, $file->getClientOriginalName() );
			$user->photo = $file->getClientOriginalName();
    	}

    	$success = $user->save();

        return response()->json(
        		[
    			'status' => 'OK',
                'message' => 'User successfully updated' 
                ], 200);
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function delete($id) {
    	$user = User::find($id);

    	if ($user == NULL) {
   			return response()->json(
   				[
   				'status' => 'ERROR',
   				'message' => 'User does not exist'
   				], 400);
   		}

    	$user->delete();

        return response()->json(
        		[
        		'status' => 'OK',
   				'message' => 'User successfully deleted'
   				], 201);
    }
}
