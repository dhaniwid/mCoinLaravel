<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Hash;
use JWTAuth;

class APIController extends Controller
{
	 
    public function login (Request $request)
    {
    	$input = $request->all();

    	if (!$token = JWTAuth::attempt($input)) {
            return response()->json(['status' => 'ERROR', 'message' => 'Invalid username or password']);
        }

        return response()->json(['status' => 'OK', 'token' => $token]);
    }
}